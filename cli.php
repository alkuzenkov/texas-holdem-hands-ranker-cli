<?php

use Ranker\App;

require_once 'vendor/autoload.php';

App::i()->runFromFile($argv[1] ?? '');
